class Rectangle
{
   private double length;
   private double width;

   public double Length { get => length; set { if (value > 0.0) length = value; } }
   public double Width { get => width; set { if (value > 0.0) width = value; } }

   public double GetArea()
   {
      return Length * Width;
   }
}